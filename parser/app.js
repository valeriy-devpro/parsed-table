const fs = require('fs');
const request = require('request');
const MongoClient = require('mongodb').MongoClient;
const crypto = require('crypto');
const diff = require('deep-diff').diff;
const util = require('util')
const NcesEdGovParser = require('./src/nces.ed.gov');

const mongodb = MongoClient.connect("mongodb://db:27017")
  .then(connection => connection.db('parser'))
  .catch(console.error);

const someSecretSalt = '1qa2ws3ed';
const url = 'https://nces.ed.gov/programs/digest/d16/tables/dt16_205.40.asp?current=yes';
request({
  method: 'GET',
  url,
}, function(err, response, body) {
  if (err) return console.error(err);
  const result = NcesEdGovParser.parse(body);

  let sign = crypto.createHmac('sha256', someSecretSalt)
    .update(JSON.stringify(result))
    .digest('hex');
  const resultToSave = Object.assign({}, result, { sign, url });
  
  fs.writeFile(`${__dirname}/results/resultFromSite.json`, JSON.stringify(resultToSave), (err) => {
    if (err) throw err;
    console.log('The file has been saved!');
  });

  mongodb
  .then(db => {
    return db.collection('parsedDocs').createIndex({ sign: 1 }, { unique: 1 })
      .then(result => db);
  })
  .then(db => db.collection('parsedDocs').save(resultToSave))
  .then(result => {
    console.log('Document has been saved');
  })
  .catch((e) => {
    if (e.code === 11000) {
      console.error('Document already exists');
    } else {
      console.log(e);
    }
  });

  const changedBody = fs.readFileSync(`${__dirname}/pageWithChanges.html`);

  const changedResult = NcesEdGovParser.parse(changedBody);

  sign = crypto.createHmac('sha256', someSecretSalt)
    .update(JSON.stringify(changedResult))
    .digest('hex');

  const changedResultToSave = Object.assign({}, changedResult, { sign, url });

  fs.writeFile(`${__dirname}/results/resultWithChanges.json`, JSON.stringify(changedResultToSave), (err) => {
    if (err) throw err;
    console.log('The file has been saved!');
  });

  mongodb
  .then(db => {
    return db.collection('parsedDocs').createIndex({ sign: 1 }, { unique: 1 })
      .then(result => db);
  })
  .then(db => db.collection('parsedDocs').save(changedResultToSave))
  .then(result => {
    console.log('Document has been saved');
  })
  .catch((e) => {
    if (e.code === 11000) {
      console.error('Document already exists');
    } else {
      console.log(e);
    }
  });

  const differences = diff(result, changedResult);

  fs.writeFile(`${__dirname}/results/diff.json`, JSON.stringify({ url, lhs: resultToSave.sign, rhs: changedResultToSave.sign, differences }), (err) => {
    if (err) throw err;
    console.log('The file has been saved!');
  });
});
const cheerio = require('cheerio');
const cheerioTableparser = require('cheerio-tableparser');
const _ = require('lodash');

class NcesEdGovParser {

  _handleData(data) {
    const rows = data.shift();
    const grouped = [];
    for(let i = 0; i < data.length; i++) {
      let [one, two, three, four, columnNumber, ...other] = data[i];
      const columnIsEmpty = other.filter((v) => v !== '');
      if (columnIsEmpty.length) {
        for(let j = (data[i].length - other.length); j < data[i].length; j++) {
          if (rows[j] !== "") {
            grouped.push({
              one, two, three, four, columnNumber,
              [rows[j]]: data[i][j]
            });
          }
        }
      }
    }
  
    return grouped;
  };

  _roots(data) {
    return _.cloneDeep(data).reduce((acc, value) => {
      const { [Object.keys(value).pop()]: lastItem } = value;
      if (lastItem === '') {
        acc.push(value);
      }
      return acc;
    }, []);
  }

  parse(body) {
    const $ = cheerio.load(body);
  
    cheerioTableparser($);
    const data = $(".tableMain").parsetable(true, true, true);

    const someObject = this._handleData(data);
  
    const rootsOfData = this._roots(someObject);
    
    let index = 0;

    someObject
      .map((val) => {
      if (_.findIndex(rootsOfData, val) !== -1) {
        index = _.findIndex(rootsOfData, val);
        rootsOfData[index][Object.keys(rootsOfData[index]).pop()] = [];
      } else {
        rootsOfData[index][Object.keys(rootsOfData[index]).pop()].push(val);
      }
  
    }, []);

    const [ footNotes, ...unused ] = $(".notes_top").parsetable(false, false, true);

    return { mainData: rootsOfData, footNotes };
  }
}

module.exports = new NcesEdGovParser();